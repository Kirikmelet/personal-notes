---
tags:
  - programming/concept
  - programming/audio
date: 2023-12-17
---
# The Basic Introduction to Audio

Computers must find a way to decode data from the audio track to a format speakers can play.
This decoded data can be expressed mathematically in a way, one example is a [Sine Wave (日本語)](20230713-正弦波.md).

## Sampling and Sample Rate

Sample rates are used by computers to convert analog input into digital input by way of sampling the data at a set rate per second.

> [!info] 例えば
> An example is (44.1k/s)

