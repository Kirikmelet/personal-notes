---
tags: politics/ideology, politics/essay
date: 2023-05-26
---

# What is Das Kapital?

Das Kapital is the fundamental building block of all Marxist theory. The book describes the relationship between commodities (goods produced by the proletariat) and
